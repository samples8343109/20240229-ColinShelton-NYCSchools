package com.cshel.nyc.domain.models

data class School(
    val dbn: String,
    val name: String,
    val phone: String,
    val email: String,
)
