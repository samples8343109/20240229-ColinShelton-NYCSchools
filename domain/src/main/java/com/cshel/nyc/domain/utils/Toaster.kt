package com.cshel.nyc.domain.utils

interface Toaster {
    fun toast(message: String, shortDuration: Boolean = true)
}