package com.cshel.nyc.domain.models

data class SatScores(
    val dbn: String,
    val schoolName: String,
    val numOfSatTestTakers: Int?,
    val satCriticalReadingAvgScore: Int?,
    val satMathAvgScore: Int?,
    val satWritingAvgScore: Int?,
)
