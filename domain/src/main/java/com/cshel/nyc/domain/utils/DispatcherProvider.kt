package com.cshel.nyc.domain.utils

import kotlinx.coroutines.CoroutineDispatcher

@Suppress("PropertyName")
interface DispatcherProvider {
    val Default: CoroutineDispatcher
    val IO: CoroutineDispatcher
    val Main: CoroutineDispatcher
    val Unconfined: CoroutineDispatcher
}