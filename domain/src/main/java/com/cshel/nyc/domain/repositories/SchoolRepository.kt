package com.cshel.nyc.domain.repositories

import com.cshel.nyc.domain.models.SatScores
import com.cshel.nyc.domain.models.School

interface SchoolRepository {
    suspend fun getSchools(): Result<List<School>>
    suspend fun getSatScores(): Result<List<SatScores>>
}