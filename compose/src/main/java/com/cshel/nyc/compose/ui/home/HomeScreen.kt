package com.cshel.nyc.compose.ui.home

import android.annotation.SuppressLint
import androidx.compose.animation.AnimatedVisibility
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.material3.Card
import androidx.compose.material3.CircularProgressIndicator
import androidx.compose.material3.HorizontalDivider
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.mutableStateOf
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import com.cshel.nyc.domain.models.School

@Composable
fun HomeScreen(state: HomeScreenState) {
    Column(
        horizontalAlignment = Alignment.CenterHorizontally,
        modifier = Modifier.fillMaxSize()
    ) {
        AnimatedVisibility(visible = state.loading.value) {
            CircularProgressIndicator(
                modifier = Modifier.width(64.dp).padding(top = 32.dp),
                color = MaterialTheme.colorScheme.secondary,
                trackColor = MaterialTheme.colorScheme.surfaceVariant,
            )
        }

        AnimatedVisibility(visible = state.schools.value.isNotEmpty()) {
            LazyColumn {
                items(state.schools.value.size) {
                    val item = state.schools.value[it]
                    Card(
                        modifier = Modifier
                            .padding(16.dp)
                            .clickable {
                                state.onClick(item.dbn)
                            }
                    ) {
                        Column(
                            Modifier
                                .padding(8.dp)
                                .fillMaxWidth()) {
                            Text(text = item.name, style = MaterialTheme.typography.headlineMedium)

                            HorizontalDivider(color = Color.Black, thickness = 1.dp)
                            Spacer(modifier = Modifier.height(8.dp))
                            Text(text = item.phone, style = MaterialTheme.typography.labelMedium)
                            Text(text = item.email, style = MaterialTheme.typography.labelMedium)
                        }
                    }
                }
            }
        }
    }
}

@SuppressLint("UnrememberedMutableState")
@Preview(showSystemUi = true)
@Composable
private fun HomeScreenPreview() {
    HomeScreen(state = HomeScreenState(
        schools = mutableStateOf(testData),
        loading = mutableStateOf(false),
        onClick = {}
    ))
}

@SuppressLint("UnrememberedMutableState")
@Preview(showSystemUi = true)
@Composable
private fun HomeScreenLoadingPreview() {
    HomeScreen(state = HomeScreenState(
        schools = mutableStateOf(emptyList()),
        loading = mutableStateOf(true),
        onClick = {}
    ))
}

val testData = listOf(
    School(
        dbn = "1",
        name = "School 1",
        phone = "888-555-1212",
        email = "school1@gmail.com"
    ),
    School(
        dbn = "2",
        name = "School 2",
        phone = "888-555-7777",
        email = "school2@yahoo.com"
    ),
    School(
        dbn = "3",
        name = "School 3",
        phone = "800-555-9999",
        email = "school3@aol.com"
    ),
)
