package com.cshel.nyc.compose.ui.detail

import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.material3.HorizontalDivider
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp

@Composable
fun DetailDialog(state: DetailState) {
    Surface(color = Color.White) {
        Column(Modifier.padding(8.dp)) {
            Text(text = state.schoolName)
            HorizontalDivider()
            Spacer(modifier = Modifier.height(8.dp))

            Row {
                Text("Reading: ")
                Text(text = state.readingScore)
            }
            Row {
                Text("Writing: ")
                Text(text = state.writingScore)
            }
            Row {
                Text("Math: ")
                Text(text = state.mathScore)
            }

        }
    }
    
}

data class DetailState(
    val schoolName: String, 
    val readingScore: String, 
    val mathScore: String, 
    val writingScore: String, 
)

@Preview(showBackground = true)
@Composable
private fun DetailDialogPreview() {
    DetailDialog(state = detailTestData)
}

val detailTestData = DetailState(
    schoolName = "School 1",
    readingScore = "700",
    writingScore = "710",
    mathScore = "720"
) 