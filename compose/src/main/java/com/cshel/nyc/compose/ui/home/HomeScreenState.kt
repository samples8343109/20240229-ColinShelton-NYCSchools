package com.cshel.nyc.compose.ui.home

import androidx.compose.runtime.State
import com.cshel.nyc.domain.models.School

data class HomeScreenState(
    val schools: State<List<School>>,
    val loading: State<Boolean>,
    val onClick: (String) -> Unit,
)