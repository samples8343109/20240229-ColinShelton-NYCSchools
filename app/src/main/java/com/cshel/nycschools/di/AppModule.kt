package com.cshel.nycschools.di

import com.cshel.nyc.domain.utils.DispatcherProvider
import com.cshel.nyc.domain.utils.DispatcherProviderImpl
import com.cshel.nyc.domain.utils.Toaster
import com.cshel.nycschools.ui.home.HomeViewModel
import com.cshel.nycschools.utils.ToasterImpl
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

val appModule = module {
    // ViewModels
    viewModel { HomeViewModel() }

    // Singletons
    single<DispatcherProvider> { DispatcherProviderImpl() }
    single<Toaster> { ToasterImpl() }
}