package com.cshel.nycschools.utils

import android.content.Context
import android.widget.Toast
import com.cshel.nyc.domain.utils.Toaster
import org.koin.core.component.KoinComponent
import org.koin.core.component.inject

class ToasterImpl: Toaster, KoinComponent {
    private val context: Context by inject()
    override fun toast(message: String, shortDuration: Boolean) {
        Toast.makeText(context.applicationContext, message, if (shortDuration) Toast.LENGTH_SHORT else Toast.LENGTH_LONG).show()
    }
}