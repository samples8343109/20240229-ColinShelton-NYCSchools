package com.cshel.nycschools.startup

import android.content.Context
import androidx.startup.Initializer
import com.cshel.nyc.data.di.dataModule
import com.cshel.nycschools.di.appModule
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.GlobalContext.startKoin

class AppStartupInitializer : Initializer<Unit> {
    override fun create(context: Context) {
        startKoin {
            androidContext(context)
            allowOverride(override = false)
            modules(
                listOf(
                    appModule,
                    dataModule,
                )
            )
        }
    }

    override fun dependencies(): List<Class<out Initializer<*>>> = emptyList()
}