package com.cshel.nycschools.ui.home

import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import com.cshel.nyc.compose.ui.home.HomeScreenState

@Composable
fun HomeViewModel.toState() = HomeScreenState(
    schools = schools.collectAsState(),
    loading = loading.collectAsState(),
    onClick = { selectedSchoolDbn.value = it }
)