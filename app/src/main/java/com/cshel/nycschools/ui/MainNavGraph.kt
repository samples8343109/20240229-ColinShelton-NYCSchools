package com.cshel.nycschools.ui

import androidx.navigation.NavController
import androidx.navigation.NavGraphBuilder
import androidx.navigation.compose.composable
import androidx.navigation.compose.dialog
import androidx.navigation.navArgument
import androidx.navigation.navigation
import com.cshel.nyc.compose.ui.detail.DetailDialog
import com.cshel.nyc.compose.ui.detail.DetailState
import com.cshel.nyc.compose.ui.home.HomeScreen
import com.cshel.nyc.compose.utils.LaunchCollection
import com.cshel.nycschools.ui.home.HomeViewModel
import com.cshel.nycschools.ui.home.toState
import org.koin.androidx.viewmodel.ext.android.getViewModel

fun NavGraphBuilder.homeComposable(navController: NavController, activity: MainActivity) {
    composable(Routes.Home) {
        val viewModel: HomeViewModel = activity.getViewModel()

        HomeScreen(state = viewModel.toState())

        viewModel.selectedSchoolScores.LaunchCollection {
            it?.let {
                navController.navigate(Routes.Details(
                    schoolName = it.schoolName,
                    readingScore = it.satCriticalReadingAvgScore?.toString() ?: "",
                    writingScore = it.satWritingAvgScore?.toString() ?: "",
                    mathScore = it.satMathAvgScore?.toString() ?: ""
                ))
            }
        }
    }
}

fun NavGraphBuilder.detailComposable(navController: NavController) {
    dialog(
        route = Routes.Details(),
        arguments = listOf(
            navArgument("schoolName") {},
            navArgument("readingScore") {},
            navArgument("mathScore") {},
            navArgument("writingScore") {},
        )
    ) { navBackStackEntry ->
        val state = DetailState(
            schoolName = navBackStackEntry.arguments?.getString("schoolName") ?: "",
            readingScore = navBackStackEntry.arguments?.getString("readingScore") ?: "",
            mathScore = navBackStackEntry.arguments?.getString("mathScore") ?: "",
            writingScore = navBackStackEntry.arguments?.getString("writingScore") ?: "",
        )

        DetailDialog(state = state)
    }
}


fun NavGraphBuilder.mainNavGraph(navController: NavController, activity: MainActivity) {
    navigation(startDestination = Routes.Home, route = Routes.Main) {
        homeComposable(navController, activity)
        detailComposable(navController)
    }
}