package com.cshel.nycschools.ui.home

import androidx.lifecycle.viewModelScope
import com.cshel.nyc.domain.models.SatScores
import com.cshel.nyc.domain.models.School
import com.cshel.nyc.domain.repositories.SchoolRepository
import com.cshel.nycschools.ui.BaseViewModel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.SharingStarted
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.combine
import kotlinx.coroutines.flow.shareIn
import org.koin.core.component.inject

class HomeViewModel: BaseViewModel() {
    // DI
    private val schoolRepository: SchoolRepository by inject()

    // Incoming API State
    private val _schools = MutableStateFlow<List<School>>(emptyList())
    private val _scores = MutableStateFlow<List<SatScores>>(emptyList())

    // Working state
    val selectedSchoolDbn = MutableStateFlow("")
    val selectedSchoolScores = combine(selectedSchoolDbn, _scores) { dbn, schools ->
        selectedSchoolDbn.value = ""
        if (dbn.isNotEmpty()) {
            schools.firstOrNull { it.dbn == dbn } ?: noScores
        } else null
    }.shareIn(viewModelScope, SharingStarted.Lazily)
    private val _loading = MutableStateFlow(false)

    // UI State
    val schools: StateFlow<List<School>> = _schools
    val loading: StateFlow<Boolean> = _loading

    init {
        loadData()
    }

    private fun loadData() {
        launchIO {
            _loading.value = true
            schoolRepository.getSchools().onSuccess {
                _schools.value = it
            }.toastException().logException()
            _loading.value = false
        }

        launchIO {
            schoolRepository.getSatScores().onSuccess {
                _scores.value = it
            }.toastException().logException()
        }
    }
    companion object {
        val noScores = SatScores(
            dbn = "",
            schoolName = "No Scores Available",
            satCriticalReadingAvgScore = null,
            satWritingAvgScore = null,
            satMathAvgScore = null,
            numOfSatTestTakers = null,
        )
    }
}