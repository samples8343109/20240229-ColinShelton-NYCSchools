package com.cshel.nycschools.ui

import android.util.Log
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.cshel.nyc.domain.utils.DispatcherProvider
import com.cshel.nyc.domain.utils.Toaster
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import org.koin.core.component.KoinComponent
import org.koin.core.component.inject

abstract class BaseViewModel : ViewModel(), KoinComponent {
    private val dispatcherProvider: DispatcherProvider by inject()
    protected val toaster: Toaster by inject()

    /**
     * Helper to launch to IO thread quickly
     */
    fun launchIO(block: suspend CoroutineScope.() -> Unit): Job =
        viewModelScope.launch(dispatcherProvider.IO, block = block)

    suspend fun runOnMain(block: suspend CoroutineScope.() -> Unit) =
        withContext(dispatcherProvider.Main, block)


    suspend fun <T> Result<T>.toastException(): Result<T> {
        runOnMain {
            if (isFailure) toaster.toast( this@toastException.exceptionOrNull()?.message ?: "Something went wrong")
        }
        return this
    }

    fun <T> Result<T>.logException(): Result<T> {
        if (isFailure) Log.d("NYC", this.exceptionOrNull()?.message ?: "Something went wrong")
        return this
    }


}