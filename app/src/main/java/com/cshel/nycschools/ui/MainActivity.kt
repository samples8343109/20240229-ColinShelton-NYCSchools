package com.cshel.nycschools.ui

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.layout.padding
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.Scaffold
import androidx.compose.material3.Text
import androidx.compose.material3.TopAppBar
import androidx.compose.ui.Modifier
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.rememberNavController
import com.cshel.nyc.compose.ui.theme.NYCSchoolsTheme

class MainActivity : ComponentActivity() {
    @OptIn(ExperimentalMaterial3Api::class)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            val navController = rememberNavController()

            NYCSchoolsTheme {
                Scaffold(
                    topBar = {
                        TopAppBar(title = { Text("NYC Schools") })
                    }
                ) {
                    NavHost(
                        navController = navController,
                        startDestination = Routes.Main,
                        modifier = Modifier.padding(it)
                    ) {
                        mainNavGraph(navController, this@MainActivity)
                    }
                }
            }
        }
    }
}
