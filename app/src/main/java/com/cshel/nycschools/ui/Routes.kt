package com.cshel.nycschools.ui


@Suppress("VariableNames")
object Routes {
    const val Main = "main"
    const val Home = "home"

    fun Details(
        schoolName: String = "{schoolName}",
        readingScore: String = "{readingScore}",
        mathScore: String = "{mathScore}",
        writingScore: String = "{writingScore}",
    ) = "details?schoolName=${schoolName}" +
            "&readingScore=${readingScore}" +
            "&mathScore=${mathScore}" +
            "&writingScore=${writingScore}"
}