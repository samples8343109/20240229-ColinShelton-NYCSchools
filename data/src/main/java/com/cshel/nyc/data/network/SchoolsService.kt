package com.cshel.nyc.data.network

import com.cshel.nyc.data.models.SatScoresDto
import com.cshel.nyc.data.models.SchoolDto
import io.ktor.client.HttpClient
import io.ktor.client.call.body
import io.ktor.client.request.get
import io.ktor.client.request.url
import org.koin.core.component.KoinComponent
import org.koin.core.component.inject

class SchoolsService : KoinComponent {
    private val client: HttpClient by inject()
    suspend fun getSchools() = runCatching {
        client.get {
            url(path = "/resource/s3k6-pzi2.json")
        }.body<List<SchoolDto>>()
    }

    suspend fun getSatScores() = runCatching {
        client.get {
            url(path = "/resource/f9bf-2cp4.json")
        }.body<List<SatScoresDto>>()
    }
}