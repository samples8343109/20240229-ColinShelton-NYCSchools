package com.cshel.nyc.data.models

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
data class SatScoresDto (
    @SerialName("dbn") val dbn: String,
    @SerialName("school_name") val schoolName: String,
    @SerialName("num_of_sat_test_takers") val numOfSatTestTakers: String,
    @SerialName("sat_critical_reading_avg_score") val satCriticalReadingAvgScore: String,
    @SerialName("sat_math_avg_score") val satMathAvgScore: String,
    @SerialName("sat_writing_avg_score") val satWritingAvgScore: String,
)
