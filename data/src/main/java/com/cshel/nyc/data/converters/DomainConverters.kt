package com.cshel.nyc.data.converters

import com.cshel.nyc.data.models.SatScoresDto
import com.cshel.nyc.data.models.SchoolDto
import com.cshel.nyc.domain.models.SatScores
import com.cshel.nyc.domain.models.School

fun SchoolDto.toSchool() = School(
    dbn = dbn,
    name = schoolName,
    email = schoolEmail ?: "",
    phone = phoneNumber
)

fun SatScoresDto.toSatScores() = SatScores(
    dbn = dbn,
    schoolName = schoolName,
    numOfSatTestTakers = numOfSatTestTakers.toIntOrNull(),
    satCriticalReadingAvgScore = satCriticalReadingAvgScore.toIntOrNull(),
    satMathAvgScore = satMathAvgScore.toIntOrNull(),
    satWritingAvgScore = satWritingAvgScore.toIntOrNull()
)