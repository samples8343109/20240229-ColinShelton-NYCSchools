package com.cshel.nyc.data.di

import com.cshel.nyc.data.network.SchoolsService
import com.cshel.nyc.data.network.ktorClient
import com.cshel.nyc.data.repositories.SchoolRepositoryImpl
import com.cshel.nyc.domain.repositories.SchoolRepository
import io.ktor.client.HttpClient
import org.koin.dsl.module

val dataModule = module {
    single<HttpClient> { ktorClient() }
    single<SchoolsService> { SchoolsService() }
    single<SchoolRepository> { SchoolRepositoryImpl() }
}