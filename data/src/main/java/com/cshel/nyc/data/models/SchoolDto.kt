package com.cshel.nyc.data.models

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
data class SchoolDto(
    @SerialName("dbn") val dbn: String,
    @SerialName("campus_name") val campusName: String? = null,
    @SerialName("city") val city: String,
    @SerialName("boys") val boys: String? = null,
    @SerialName("girls") val girls: String? = null,
    @SerialName("attendance_rate") val attendanceRate: String,
    @SerialName("grades2018") val grades2018: String,
    @SerialName("latitude") val latitude: String? = null,
    @SerialName("location") val location: String,
    @SerialName("longitude") val longitude: String? = null,
    @SerialName("phone_number") val phoneNumber: String,
    @SerialName("school_email") val schoolEmail: String? = null,
    @SerialName("school_name") val schoolName: String,
    @SerialName("total_students") val totalStudents: String,
    @SerialName("website") val website: String,
)