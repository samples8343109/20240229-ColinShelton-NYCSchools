package com.cshel.nyc.data.repositories

import com.cshel.nyc.data.converters.toSatScores
import com.cshel.nyc.data.converters.toSchool
import com.cshel.nyc.data.network.SchoolsService
import com.cshel.nyc.domain.models.SatScores
import com.cshel.nyc.domain.models.School
import com.cshel.nyc.domain.repositories.SchoolRepository
import org.koin.core.component.KoinComponent
import org.koin.core.component.inject

class SchoolRepositoryImpl : SchoolRepository, KoinComponent {
    private val schoolService: SchoolsService by inject()
    override suspend fun getSchools(): Result<List<School>> =
        schoolService.getSchools().map { it.map { it.toSchool() } }

    override suspend fun getSatScores(): Result<List<SatScores>> =
        schoolService.getSatScores().map { it.map { it.toSatScores() } }

}